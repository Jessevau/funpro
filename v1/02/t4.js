function potenssi(a, b) {
    function potenssiHelper(a, b){
        if ( b == 0 ){
            return 1;
        } else {
            return a * potenssiHelper(a, b - 1);
        }
    }
    return potenssiHelper(a, b);
}

var test1 = potenssi(5,3);
var test2 = potenssi(2,0);
var test3 = potenssi(4,4);
console.log(test1);
console.log(test2);
console.log(test3);