const f = function (a,b) {
    return function (a, b) {
		if ( a != b ){
		    if ( a > b ){
		        return 1;
		    }
		    return -1;
		}
		return 0;
	 }
	}();

let test1 = f(3, 2);
let test2 = f(1, 2);
let test3 = f(1, 1);

console.log(test1);
console.log(test2);
console.log(test3);