const f = function (a,b) {
    return function (a, b) {
		if ( a != b ){
		    if ( a > b ){
		        return 1;
		    }
		    return -1;
		}
		return 0;
	 }
}();

var helsinki15 = [-10, -7, -4, 5, 10, 20, 22, 14, 6, 1, -3, -10];
var helsinki16 = [-11, -9, -6, 5, 11, 22, 20, 15, 5, 1, -3, -9];

function vertaaja(vrt, helsinki15, helsinki16){
    var korkeampi = 0;
    for (var i = 0; i < helsinki15.length; i++) {
        if ( vrt(helsinki15[i], helsinki16[i]) == -1 ){
            korkeampi++;
        }
    }
    return korkeampi;
}

console.log(vertaaja(f, helsinki15, helsinki16));