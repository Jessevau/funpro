const arr = [1,2,3,4,5,6,7,8,9];
const reversed = reverse(arr);

function reverse(arr, acc = []) {
  if (arr.length) {
    const [ head, ...tail ] = arr;
    return reverse(tail, [head].concat(acc));
  } else {
    return acc;
  }
};

console.log(reversed);