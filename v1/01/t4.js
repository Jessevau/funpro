function korotus(luku, eksponentti){
    if ( eksponentti == 0 ){
        return 1;
    } else {
        return luku * korotus(luku, eksponentti - 1);
    }
};

var tarkastus = function(a, b){
    console.log("Potenssiin korotus "+ a +" potenssiin "+ b);
    console.log(korotus(a, b));
};

tarkastus(2, 2);
tarkastus(5, 3);
tarkastus(2, 0);