var onPalindromi = function(str){
    if ( str.length < 2 ){
        return true;
    }
    
    if (str.slice(0, 1) == str.slice(-1)) {
        return onPalindromi(str.slice(1, -1));
    }
    
    return false;
};

var tarkastus = function(str){
    console.log("Onko palindromi? "+ str);
    console.log(onPalindromi(str));
};

tarkastus("a");
tarkastus("sokos");
tarkastus("sdfsdf");