var gcd = function(p, q) {
    if ( q == 0 ) {
        return p;
    }
    
    return gcd(q, p % q);
};

var tarkastus = function(p, q){
    console.log("Suurin yhteinen tekijä luvuille "+ p +" ja "+ q);
    console.log(gcd(p, q));
};

tarkastus(102, 68);
tarkastus(50, 10);
tarkastus(3, 2);