var kjl = function(p, q) {
    if ( q == 0 ) {
        return p;
    }
    
    return kjl(q, p % q) == 1;
};

var tarkastus = function(p, q){
    console.log("Ovatko keskenään jaottomia "+ p +" ja "+ q);
    console.log(kjl(p, q));
};

tarkastus(35, 18);
tarkastus(50, 10);
tarkastus(3, 2);
tarkastus(100,99);