package menu;

import java.util.function.Function;

public class Tehtava1 {
    public static void main(String[] args){
        Function<Integer,Double> FtoC = x -> new Double((x-32)/1.8);
        double fahrenheit = FtoC.apply(100);
        System.out.println("testi "+fahrenheit);
        
        Function<Integer,Double> radius = x -> new Double(3.14159*x*x);
        double area = radius.apply(40);
        System.out.println("area "+area);
    }

}