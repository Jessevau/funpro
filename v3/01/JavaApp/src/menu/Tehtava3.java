package menu;

import java.util.function.Function;

public class Tehtava3 {
    public static void main(String[] args){
        IntStream.generate(() -> ThreadLocalRandom.current().nextInt(6)+1).limit(20).forEach(System.out::println);
    }

}