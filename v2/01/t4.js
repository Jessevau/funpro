var helsinki15 = [-10, -7, -4, 5, 10, 20, 22, 14, 6, 1, -3, -10];
var helsinki16 = [-11, -9, -6, 5, 11, 22, 20, 15, 5, 1, -3, -9];

var helsinkiKA = [];

for (var i = 0; i < helsinki15.length; i++) {
    var ka = (helsinki15[i]+helsinki16[i])/2;
    helsinkiKA.push(ka);
}

const positiiviset = helsinkiKA.filter(x=>x>=0);

var sum, avg = 0;
if(positiiviset.length){
    sum = positiiviset.reduce(function(a,b){ return a+b; });
    avg = sum / positiiviset.length;
}

console.log(helsinkiKA);
console.log(positiiviset);
console.log(avg);