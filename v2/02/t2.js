const Auto  = (function(){
    
    const suojatut = new WeakMap();  // yhteinen ilmentymille
    
    class Auto{
        constructor(p_tankki, p_matkamittari){
            suojatut.set(this, {matkamittari: p_matkamittari});
            this.tankki = p_tankki;
        }  
        
     
        getMatkamittari() {return suojatut.get(this).matkamittari;}
        getTankki() { return this.tankki;}
        
        aja() { suojatut.get(this).matkamittari++, this.tankki--;}
        lisääLöpöä(x) { this.tankki += x; }
        
        getMap() {return suojatut;}  // testausta varten
        
    }
    
    return Auto;
})();

const auto = new Auto(30, 0 );
auto.aja();
auto.aja();
auto.aja();
auto.aja();
auto.aja();
auto.lisääLöpöä(10);
console.log(auto.getMatkamittari());
console.log(auto.getTankki());