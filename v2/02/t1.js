function laskePisteet(pituus){
    return function(lisaPisteet){
        if ( lisaPisteet == 1.8 && pituus <= 99 ){
            var apu = pituus - 75;
            pituus = pituus + (apu * lisaPisteet);
            return pituus;
        } else if ( lisaPisteet == 2.0 && pituus >= 100 ) {
            var apu2 = pituus - 100;
            pituus = pituus + (apu2 * lisaPisteet);
            return pituus;
        }
    }
}

console.log(laskePisteet(98)(1.8));